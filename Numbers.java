
public class Numbers {

public static void main(String[] args) {
	int[] list = new int[10];
	System.out.println("List of numbers:");

	for (int i = 0; i < list.length; i++) {
	list[i] = (int)(Math.random() * 100);
	System.out.print(list[i] + " ");

	}

	System.out.println("\n\nSmallest number in list:");
	System.out.println(min(list));

	}

public static int min(int[] list) {
	return min(list, 0, list.length - 1);

	}

public static int min(int[] list, int low, int high) {
	if (low == high) {
	return list[low];
	} else if (low + 1 == high) {
	return Math.min(list[low], list[high]);
	} else {
	int mid = (low + high) / 2;
	return Math.min(min(list, low, mid), min(list, mid, high));

	}
	}

}
